#!/usr/bin/env node
var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var app = express();

// Set static files
app.use(express.static('app'));
app.use(express.static('.tmp'));
app.use(express.static(path.join(__dirname, 'client')));
app.use('/static', express.static('public'));
app.use('/bower_components', express.static(path.dirname(__dirname) + '/payment/bower_components'));

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

// Set routes
require('./server/routes/user.route')(app);
require('./server/routes/stripe.route')(app);
require('./server/routes/backup.route')(app);
require('./server/routes/video.route')(app);
require('./server/routes/news.route')(app);
require('./server/routes/liveVideo.route')(app);
require('./server/routes/recording.route')(app);
require('./server/routes/ytAndOther.route')(app);
require('./server/routes/slider.route')(app);
require('./server/routes/scores.route')(app);
require('./server/routes/paypal.router')(app);
require('./server/routes/terms.route')(app);
require('./server/routes/schedule.route')(app);
require('./server/routes/coupon.route')(app);
require('./server/routes/subscription.route')(app);
require('./server/routes/upcoming.route')(app);
require('./server/routes/email.router')(app);

// SSL Cert
var fs = require('fs');
ca = [];
chain = fs.readFileSync('/root/ssl/fijifootballlive.com.crt.bundle', 'utf8');
chain = chain.split('\n');
cert = [];
for (var i = 0; i < chain.length; i++) {
    line = chain[i];
    if (line.length > 0) {
        cert.push(line);
        if (line.match(/-END CERTIFICATE-/)) {
            ca.push(cert.join('\n'));
            cert = [];
        }
    }
}
var options = {
    ca: ca,
    key: fs.readFileSync('/root/ssl/fijifootballlive.com.key', 'utf8'),
    cert: fs.readFileSync('/root/ssl/fijifootballlive.com.crt', 'utf8')
};

// HTTPS Server
require('https').createServer(options, app).listen(443);

// HTTP -> HTTPS
require('http').createServer(function (req, res) {
    res.writeHead(301, {"Location": "https://" + req.headers['host'] + req.url});
    res.end();
}).listen(80);

/* app.set('port', process.env.PORT || 3000);

 var server = app.listen(app.get('port'), function () {
 console.log('Express server listening at http://localhost:' + server.address().port);
 });*/
