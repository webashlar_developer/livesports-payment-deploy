exports.host = "wmaxatv.s3.amazonaws.com";

exports.videoApi = "/fijis/video.json";
exports.newsApi = "/fijis/news.json";
exports.termsApi = "/fijis/terms.txt";
exports.slideApi = "/fijis/slider.json";
exports.scoresApi = "/fijis/scores.json";
exports.recordingsApi = "/fijis/recordings.json";
exports.liveApi = "/fijis/11/live.json";
exports.scheduleApi = "/fijis/11/schedule.json";
exports.couponApi = "/fijis/coupon.json";
exports.youtubeApi = "/fijis/videos.json";
exports.upcomingApi = "/fijis/upcoming.json";
exports.subscriptionApi = "/fijis/subscription.json";

exports.mainAppUrl = 'http://www.fijifootballlive.com/#/'; // URL for redirect on main(video) app

exports.stripe_sk_live = 'sk_live_4Wx8FRwvl3OCP7EMMLW0BxeA';
exports.stripe_sk_test = 'sk_test_1t791F9jujclH24VvQp9hazh';


/*--- Email Receipt config ---*/
exports.emailServiceProvider = {
    service: "Gmail", // Service provider
    user: "fijifootballlive@gmail.com", //Sender email account ID (Ex: 'fijifootballlive@gmail.com')
    pass: "fijifootballlive897" //email account password (Ex: Password of 'fijifootballlive@gmail.com' account)
};
exports.emailReceiptMessage = {
    from: 'Fiji Football Live <fijifootballlive@gmail.com>', // sender address.  Must be the same as authenticated user if using Gmail.
    subject: 'Fiji Football Payment Receipt', // Email subject
    html: '<h2>You have successfully paid for Vodafone Fiji Fact Cup Subscription Package</h2>' // Email Template
};

