var http = require('http');
var constant = require('../constant.js');

exports.getVideos = function () {
    return new Promise(function (resolve, reject) {
        var data = '';
        var options = {
            host: constant.host,
            path: constant.recordingsApi,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        };

        var req = http.request(options, function (res) {
            var output = '';
            res.setEncoding('utf8');

            res.on('data', function (chunk) {
                output += chunk;
            });

            res.on('end', function () {
                try {
                    var obj = JSON.parse(output.trim());
                    resolve(fnAddIds(obj.items));
                } catch (e) {
                    var obj = {"items": []};
                    resolve(fnAddIds(obj.items));
                    return console.error(e);
                }
            });
        }).end();


    });
};

/*
exports.getVideos = function () {
    return new Promise(function (resolve, reject) {
        var data = {
            "version": "1",
            "items": [
                {
                    "date": "1453154400000",
                    "title": "Medal - Rugby Games Olympic",
                    "subtitle": "Medal - Rugby Games Olympic",
                    "text": "Medal - Rugby Games Olympic",
                    "duration": "39",
                    "thumb":"http://wpc.2AAD8.alphacdn.net/802AAD8/liveegvqregv/voddvr/gold1.jpg",
                    "video":"http://50.7.68.146/decryptedFiji/Movies/Decrypted/medal/playlist.m3u8"
                },
                {
                    "date": "1469653200000",
                    "title": "Final - Rugby Games Olympic",
                    "subtitle": "Final - Rugby Games Olympic",
                    "text": "Final - Rugby Games Olympic",
                    "duration": "99",
                    "thumb":"http://wpc.2AAD8.alphacdn.net/802AAD8/liveegvqregv/voddvr/fijijfinal.jpg",
                    "video":"http://50.7.68.146/decryptedFiji/Movies/Decrypted/final/playlist.m3u8"
                },
                {
                    "date": "1469653200000",
                    "title": "Fiji vs Japan_Sami Final 1",
                    "subtitle": "Fiji vs Japan_Sami Final 1 - Rugby Games Olympic",
                    "text": "Fiji vs Japan_Sami Final 1 - Rugby Games Olympic",
                    "duration": "5180",
                    "thumb":"http://1351882701.rsc.cdn77.org/vod/FijivsJapansami2016.jpg",
                    "video":"http://50.7.68.146/decryptedFiji/Movies/Decrypted/semi1/playlist.m3u8"
                },
                {
                    "date": "1469480400000",
                    "title": "England vs South Africa_Sami Final",
                    "subtitle": "England vs South Africa_Sami Final - Rugby Games Olympic",
                    "text": "England vs South Africa_Sami Final - Rugby Games Olympic",
                    "duration": "58",
                    "thumb":"http://1351882701.rsc.cdn77.org/vod/EngvsSau2016.jpg",
                    "video":"http://50.7.68.146/decryptedFiji/Movies/Decrypted/semi2/playlist.m3u8"
                },
                {
                    "date": "1469480400000",
                    "title": "Fiji vs New Zealand",
                    "subtitle": "Fiji vs New Zealand - Rugby Games Olympic",
                    "text": "Fiji vs New Zealand - Rugby Games Olympic",
                    "duration": "106",
                    "thumb":"http://1351882701.rsc.cdn77.org/live/rio2016olympic121.jpg",
                    "video":"http://50.7.68.146/decryptedFiji/Movies/Decrypted/quater/playlist.m3u8"
                }
            ]
        };
        resolve(fnAddIds(data.items));
    });
};*/

function fnAddIds(data) {
    var len = data.length, obj;

    for (var intIndex = 0; intIndex < len; intIndex++) {
        obj = data[intIndex];
        obj.id = intIndex + 1;
    }

    return data;
}
