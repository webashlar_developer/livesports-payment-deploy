const nodemailer = require('nodemailer');
var constant = require('../constant.js');


exports.sendReceipt = function (receiverEmail) {
    var smtpTransport = nodemailer.createTransport({
        service: constant.emailServiceProvider.service,  // sets automatically host, port and connection security settings
        auth: {
            user: constant.emailServiceProvider.user,
            pass: constant.emailServiceProvider.pass
        },
        tls: {
            rejectUnauthorized: false
        }
    });

    return new Promise(function (resolve, reject) {
        smtpTransport.sendMail({  //email options
            from: constant.emailReceiptMessage.from, // sender address.  Must be the same as authenticated user if using Gmail.
            to: "< " + receiverEmail + ">", // receiver
            subject: constant.emailReceiptMessage.subject, // subject
            text: "Successfully Paid", // body
            html: constant.emailReceiptMessage.html
        }, function (error, response) {  //callback
            if (error) {
                reject(error);
            } else {
                resolve("Message sent: " + response.message);
            }

            smtpTransport.close(); // shut down the connection pool, no more messages.  Comment this line out to continue sending emails.
        });
    });
};
