var http = require('http');
var constant = require('../constant.js');

exports.getSchedule = function () {
    return new Promise(function (resolve, reject) {
        var data = '';
        var options = {
            host: constant.host,
            path: constant.scheduleApi,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        };

        var req = http.request(options, function (res) {
            var output = '';
            res.setEncoding('utf8');

            res.on('data', function (chunk) {
                output += chunk;
            });

            res.on('end', function () {
                try {
                    var arr = JSON.parse(output.trim());
                    resolve(arr);
                } catch (e) {
                    var arr = [];
                    resolve(arr);
                    return console.error(e);
                }
            });
        }).end();


    });
};
