var constant = require('../constant.js');
var stripe = require('stripe')(constant.stripe_sk_live);
// var stripe = require('stripe')(constant.stripe_sk_test);

// apply charge
exports.stripeCharge = function (chargeObj) {
    return new Promise(function(resolve, reject) {
        stripe.charges.create(chargeObj,
            function (err, charge) {
                if (err) {
                    reject(err);
                }
                resolve(charge);
            });
    });
};

// create new stripe customer
exports.createStripeCustomer = function (customerObj) {
    return new Promise(function(resolve, reject) {
        stripe.customers.create(customerObj,
            function (err, customer) {
                if (err) {
                    reject(err);
                }
                resolve(customer);
            });
    });
};

//create new stripe subscriptions for existing customer
exports.createStripeSubscriptions = function (subscriptionsObj) {
    return new Promise(function(resolve, reject) {
        stripe.subscriptions.create(subscriptionsObj,
            function (err, customer) {
                if (err) {
                    reject(err);
                }
                resolve(customer);
            });
    });
};


//delete stripe subscriptions
exports.deleteStripeSubscriptions = function (subscriptionsId) {
    return new Promise(function(resolve, reject) {
        stripe.subscriptions.del(subscriptionsId,
            function (err, data) {
                if (err) {
                    reject(err);
                }
                resolve(data);
            });
    });
};
