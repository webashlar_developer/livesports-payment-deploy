//process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
var http = require('http');
var request = require('request');
var mac = require('getmac');
var constant = require('../constant.js');

exports.getVideos = function () {
    return new Promise(function (resolve, reject) {
        var data = '';
        var options = {
            host: constant.host,
            path: constant.liveApi,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        };

        var req = http.request(options, function (res) {
            var output = '';
            res.setEncoding('utf8');

            res.on('data', function (chunk) {
                output += chunk;
            });

            res.on('end', function () {
                try {
                    var obj = JSON.parse(output.trim());
                    resolve(fnAddIds(obj));
                } catch (e) {
                    var obj = {"items": []};
                    resolve(fnAddIds(obj));
                    return console.error(e);
                }
            });
        }).end();

    });
};

exports.getSignature = function (email, ip) {
    return new Promise(function (resolve, reject) {
        var auth = "Basic " + new Buffer('admin' + ":" + 'qaz890').toString("base64");
        var ipKey = ip.replace(/[.]/g, "");

        var json = {"email": email, "devicetype": getDeviceType(), "deviceid": ipKey,"ip": ip};

        var options = {
            url: 'https://getipv4address.com/cdn/',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': auth
            },
            json: json
        };
        request(options, function (err, res, body) {
            if (err) {
                resolve(err);
            } else {
                if (res && (res.statusCode === 200 || res.statusCode === 201)) {
                    resolve(body);
                }
            }
        });

    });
};

function fnAddIds(data) {
    var len = data.items.length, obj;

    for (var intIndex = 0; intIndex < len; intIndex++) {
        obj = data.items[intIndex];
        obj.id = intIndex + 1;
        obj.email = {
            "email_display" : data['email_display'],
            "email_time_display" : data['email_time_display'],
            "email_duration" : data['email_duration'],
            "email_location" : data['email_location']
        }
    }

    return data;
}

function getDeviceType() {
    var type = null;
    switch (process.platform) {
        case "win32":
            type = 'windows pc';
            break;
        case "darwin":
            type = 'mac';
            break;
        default:
            type = process.platform;
    }
    return type;
}
