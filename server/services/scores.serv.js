var http = require('http');
var constant = require('../constant.js');

exports.getScores = function () {
    return new Promise(function (resolve, reject) {
        var data = '';
        var options = {
            host: constant.host,
            path: constant.scoresApi,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        };

        var req = http.request(options, function (res) {
            var output = '';
            res.setEncoding('utf8');

            res.on('data', function (chunk) {
                output += chunk;
            });

            res.on('end', function () {
                try {
                    var obj = JSON.parse(output.trim());
                    resolve(obj.items);
                } catch (e) {
                    var obj = {"items": []};
                    resolve(obj.items);
                    return console.error(e);
                }
            });
        }).end();


    });
};
