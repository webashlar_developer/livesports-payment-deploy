var $firebaseCSVService = require("firebase-to-csv");

// apply charge
exports.fireBackup = function (req, res) {

    return new Promise(function(resolve, reject) {
        function fnCheck(isTrue){
            if(isTrue){
                resolve('user_profile.csv')
            }
        };

        fnCheck(true);

        $firebaseCSVService('https://livesports-77d20.firebaseio.com/user_profile', fnCheck, 'public/user_profile.csv');
        //$firebaseCSVService('https://livesports-77d20.firebaseio.com/user_Charges', fnCheck, 'public/user_Charges.csv');
    });

};
