var http = require('http');
var constant = require('../constant.js');

exports.getTerms = function () {
    return new Promise(function (resolve, reject) {
        var data = '';
        var options = {
            host: constant.host,
            path: constant.termsApi,
            method: 'GET',
            headers: {
                'Content-Type': 'text/plain'
            }
        };

        var req = http.request(options, function (res) {
            var output = '';
            res.setEncoding('utf8');

            res.on('data', function (chunk) {
                output += chunk;
            });

            res.on('end', function () {
                var obj = output.trim();
                resolve(obj);
            });
        }).end();


    });
};
