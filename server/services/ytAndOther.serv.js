var http = require('http');
var constant = require('../constant.js');

exports.getVideos = function () {
    return new Promise(function (resolve, reject) {

        var data = '';
        var options = {
            host: constant.host,
            path: constant.youtubeApi,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        };

        var req = http.request(options, function (res) {
            var output = '';
            res.setEncoding('utf8');

            res.on('data', function (chunk) {
                output += chunk;
            });

            res.on('end', function () {
                try {
                    var obj = JSON.parse(output.trim());
                    resolve(fnAddIds(obj.items));
                } catch (e) {
                    var obj = {"items": []};
                    resolve(fnAddIds(obj.items));
                    return console.error(e);
                }
            });
        }).end();

    });
};

function fnAddIds(data) {
    var len = data.length, obj;

    for (var intIndex = 0; intIndex < len; intIndex++) {
        obj = data[intIndex];
        obj.id = intIndex + 1;
    }

    return data;
}
