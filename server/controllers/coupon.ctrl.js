var couponServ = require('../services/coupon.serv.js');

exports.getCoupon = function (req, res) {
    couponServ.getCoupon()
        .then(function (coupon) {

            for (var i = 0; i < coupon.length; ++i) {
                //convert json in string
                var cts = JSON.stringify(coupon[i]);
                var couponToStr = JSON.stringify(cts);

                //convert json string to base64
                var ctsBuf = new Buffer(couponToStr);
                var ctsB64 = ctsBuf.toString('base64');
                coupon[i] = ctsB64;
            }

            res.send(coupon);
        }, function (err) {
            res.status(err.raw.statusCode).send(err);
        });
};
