var videoServ = require('../services/liveVideo.serv.js');

// get videos list
exports.getVideos = function (req, res) {
    videoServ.getVideos()
        .then(function (videos) {
            res.send(videos);
        }, function (err) {
            res.status(err.raw.statusCode).send(err);
        });
};

// get single video
exports.getVideo = function (req, res) {
    videoServ.getVideos()
        .then(function (videos) {
            var videos = videos.items;
            var index = videos.map(function (obj) {
                return (obj.id).toString();
            }).indexOf(req.params.id);
            if (index > -1) {
                res.send(videos[index]);
            } else {
                res.status(404).send({
                    status: 404,
                    error: 'not found'
                });
            }
        }, function (err) {
            res.status(err.raw.statusCode).send(err);
        });
};

// get videos list
exports.getSignature = function (req, res) {
    videoServ.getSignature(req.params.email, req.params.ipKey)
        .then(function (videos) {
            res.send(videos);
        }, function (err) {
            res.status(err.raw.statusCode).send(err);
        });
};


