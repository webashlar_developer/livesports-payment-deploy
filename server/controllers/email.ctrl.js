var emailServ = require('../services/email.serv.js');

// get videos list
exports.sendPaymentReceipt = function (req, res) {
    emailServ.sendReceipt(req.params.email)
        .then(function (response) {
            res.send(response);
        }, function (err) {
            res.status(err.raw.statusCode).send(err);
        });
};
