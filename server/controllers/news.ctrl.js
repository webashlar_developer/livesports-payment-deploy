var newsServ = require('../services/news.serv.js');

// get news list
exports.getNews = function (req, res) {
    newsServ.getNews()
        .then(function (news) {
            res.send(news);
        }, function (err) {
            res.status(err.raw.statusCode).send(err);
        });
};

// get single news
exports.getSingleNews = function (req, res) {
    newsServ.getNews()
        .then(function (news) {
            var index = news.map(function (obj) {
                return (obj.id).toString();
            }).indexOf(req.params.id);
            if (index > -1) {
                res.send(news[index]);
            } else {
                res.status(404).send({
                    status: 404,
                    error: 'not found'
                });
            }
        }, function (err) {
            res.status(err.raw.statusCode).send(err);
        });
};

