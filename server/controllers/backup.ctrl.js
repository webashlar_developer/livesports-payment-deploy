var backupServ = require('../services/backup.serv.js');
var path = require('path');
var fs = require("fs");
var mime = require('mime');


// create new stripe customer
exports.backup = function (req, res) {
    backupServ.fireBackup()
        .then(function (filename) {
            var filePath = '/static/' + filename;
            res.send(filePath);
        }, function (err) {
            res.status(err.raw.statusCode).send(err);
        });
};
