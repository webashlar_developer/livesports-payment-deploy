var scoresServ = require('../services/scores.serv.js');

exports.getScores = function (req, res) {
    scoresServ.getScores()
        .then(function (videos) {
            res.send(videos);
        }, function (err) {
            res.status(err.raw.statusCode).send(err);
        });
};


