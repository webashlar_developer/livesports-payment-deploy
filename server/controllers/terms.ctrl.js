var termsServ = require('../services/terms.serv.js');

exports.getTerms = function (req, res) {
    termsServ.getTerms()
        .then(function (terms) {
            res.send(terms);
        }, function (err) {
            res.status(err.raw.statusCode).send(err);
        });
};


