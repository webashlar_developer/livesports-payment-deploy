var upcomingServ = require('../services/upcoming.serv.js');

exports.getUpcoming = function (req, res) {
    upcomingServ.getUpcoming()
        .then(function (upcoming) {
            res.send(upcoming);
        }, function (err) {
            res.status(err.raw.statusCode).send(err);
        });
};
