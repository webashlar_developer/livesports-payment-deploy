var scheduleServ = require('../services/schedule.serv.js');

exports.getSchedule = function (req, res) {
    scheduleServ.getSchedule()
        .then(function (videos) {
            res.send(videos);
        }, function (err) {
            res.status(err.raw.statusCode).send(err);
        });
};


