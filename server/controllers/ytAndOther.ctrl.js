var ytAndOtherServ = require('../services/ytAndOther.serv.js');

// get videos list
exports.getVideos = function (req, res) {
    ytAndOtherServ.getVideos()
        .then(function (videos) {
            res.send(videos);
        }, function (err) {
            res.status(err.raw.statusCode).send(err);
        });
};

// get single video
exports.getVideo = function (req, res) {
    ytAndOtherServ.getVideos()
        .then(function (videos) {
            var index = videos.map(function (obj) {
                return (obj.id).toString();
            }).indexOf(req.params.id);
            if (index > -1) {
                res.send(videos[index]);
            } else {
                res.status(404).send({
                    status: 404,
                    error: 'not found'
                });
            }
        }, function (err) {
            res.status(err.raw.statusCode).send(err);
        });
};

