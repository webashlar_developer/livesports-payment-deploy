var sliderServ = require('../services/slider.serv.js');

exports.getSlides = function (req, res) {
    sliderServ.getSlides()
        .then(function (videos) {
            res.send(videos);
        }, function (err) {
            res.status(err.raw.statusCode).send(err);
        });
};


