var subscriptionServ = require('../services/subscription.serv.js');

exports.getSubscription = function (req, res) {
    subscriptionServ.getSubscription()
        .then(function (subscription) {
            res.send(subscription);
        }, function (err) {
            res.status(err.raw.statusCode).send(err);
        });
};


