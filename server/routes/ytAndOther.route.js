'use strict';

var ytAndOtherCtrl = require('../controllers/ytAndOther.ctrl');

module.exports = function (app) {
    app.route('/ytAndOthers')
        .get(ytAndOtherCtrl.getVideos);

    app.route('/ytAndOthers/:id')
        .get(ytAndOtherCtrl.getVideo);
};
