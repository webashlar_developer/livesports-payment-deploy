'use strict';

var sliderCtrl = require('../controllers/slider.ctrl');

module.exports = function (app) {
    app.route('/sliders')
        .get(sliderCtrl.getSlides);
};
