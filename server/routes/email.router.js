'use strict';

var emailCtrl = require('../controllers/email.ctrl');

module.exports = function (app) {
    app.route('/receipt/:email')
        .get(emailCtrl.sendPaymentReceipt);
};
