'use strict';

var paypalCtrl = require('../controllers/paypal.ctrl');

module.exports = function (app) {
    app.route('/paypal')
        .post(paypalCtrl.fnPay);
    app.route('/paypal')
        .get(paypalCtrl.fnPay);
};
