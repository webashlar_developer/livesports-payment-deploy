'use strict';

var backupCtrl = require('../controllers/backup.ctrl');

module.exports = function (app) {
    app.route('/backup')
        .get(backupCtrl.backup);
};
