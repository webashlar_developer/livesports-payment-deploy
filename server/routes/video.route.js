'use strict';

var videoCtrl = require('../controllers/video.ctrl');

module.exports = function (app) {
    app.route('/videos')
        .get(videoCtrl.getVideos);

    app.route('/videos/:id')
        .get(videoCtrl.getVideo);
};
