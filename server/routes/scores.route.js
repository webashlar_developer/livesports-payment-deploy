'use strict';

var scoresCtrl = require('../controllers/scores.ctrl');

module.exports = function (app) {
    app.route('/scores')
        .get(scoresCtrl.getScores);
};
