'use strict';

var newsCtrl = require('../controllers/news.ctrl');

module.exports = function (app) {
    app.route('/news')
        .get(newsCtrl.getNews);

    app.route('/news/:id')
        .get(newsCtrl.getSingleNews);
};
