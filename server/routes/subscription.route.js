'use strict';

var subscriptionCtrl = require('../controllers/subscription.ctrl');

module.exports = function (app) {
    app.route('/subscription')
        .get(subscriptionCtrl.getSubscription);
};
