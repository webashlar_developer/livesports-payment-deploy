'use strict';

var videoCtrl = require('../controllers/liveVideo.ctrl');

module.exports = function (app) {
    app.route('/liveVideos')
        .get(videoCtrl.getVideos);

    app.route('/liveVideos/:id')
        .get(videoCtrl.getVideo);

    app.route('/getSignature/:email/:ipKey')
        .get(videoCtrl.getSignature);
};
