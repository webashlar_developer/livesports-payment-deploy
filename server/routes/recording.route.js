'use strict';

var videoCtrl = require('../controllers/recording.ctrl');

module.exports = function (app) {
    app.route('/recordings')
        .get(videoCtrl.getVideos);

    app.route('/recordings/:id')
        .get(videoCtrl.getVideo);
};
