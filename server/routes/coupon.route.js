'use strict';

var couponCtrl = require('../controllers/coupon.ctrl');

module.exports = function (app) {
    app.route('/Y291cG9u')
        .get(couponCtrl.getCoupon);
};
