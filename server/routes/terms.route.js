'use strict';

var termsCtrl = require('../controllers/terms.ctrl');

module.exports = function (app) {
    app.route('/terms')
        .get(termsCtrl.getTerms);
};
