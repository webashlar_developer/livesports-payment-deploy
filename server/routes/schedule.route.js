'use strict';

var scheduleCtrl = require('../controllers/schedule.ctrl');

module.exports = function (app) {
    app.route('/schedule')
        .get(scheduleCtrl.getSchedule);
};
