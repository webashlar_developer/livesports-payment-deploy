'use strict';

var upcomingCtrl = require('../controllers/upcoming.ctrl');

module.exports = function (app) {
    app.route('/upcoming')
        .get(upcomingCtrl.getUpcoming);
};
